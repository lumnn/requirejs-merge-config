#!/usr/bin/env node

const merge = require('./index')

var args = process.argv.slice(2)

if (!args[0]) {
  throw new Error("require js config path must be provided as first argument")
}

var filename = args[0]
if (filename[0] !== '/') {
  filename = process.cwd() + '/' + filename
}

var entireConfig = merge(filename)

process.stdout.write("var require = " + JSON.stringify(entireConfig, null, 2))
