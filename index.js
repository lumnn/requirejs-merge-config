const _ = require('lodash')

module.exports = function merge(filename) {
  var entireConfig = {
    deps: [],
  }

  var newRequire = function () {}
  newRequire.config = function (config) {
    entireConfig.deps.push(...config.deps || [])
    delete config.deps
    _.merge(entireConfig, config)
  }

  var SandboxedModule = require('sandboxed-module');
  SandboxedModule.require(filename, {
    locals: {
      require: newRequire,
      define: function () {},
    },
  });

  return entireConfig
}
