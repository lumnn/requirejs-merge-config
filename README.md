# RequireJS Merge Config

> A simple tool to merge requierjs config file into single object

The only use case for this module is with `requirejs-config.js` generated in
Magento from smaller configs

## Usage

```sh
npx requirejs-merge-config path/to/requirejs-config.js > requirejs-config-merged.js
```

## How it works

It executes the passed file as JS with changed `require` variable, so everytime
that file executes `require.config` function it collects the config information,
and then it outputs it in following format

```js
var require = JSON_STRINGIFED_CONFIG
```
